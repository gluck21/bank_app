package com.example.dagicore_bank_app.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
    String accountName;
    String accountPassword;
    Double initialDeposit;
}
