package com.example.dagicore_bank_app.models;

import com.example.dagicore_bank_app.enums.Roles;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
    private String accountName;
    private String accountPassword;
    private String accountNumber;
   private List<AccountStatement>accountStatementList = new ArrayList<>();
   private static Double accountBalance=0.00;
    Roles roles;
    private Boolean isEnabled;
    public Double  addToBalance(Double amount){
        accountBalance+=amount;
       return  accountBalance;
    }
    public Double  withDraw(Double amount){
        accountBalance-=amount;
        return  accountBalance;
    }
    public Double checkbalance(){
        return accountBalance;
    }

}
