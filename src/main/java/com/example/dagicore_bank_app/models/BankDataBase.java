package com.example.dagicore_bank_app.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.*;
@Component
@Data
@NoArgsConstructor
public class BankDataBase {

 static Map<String, User> userList = new HashMap<>();
 public void addUser(User user){
  userList.put(user.getAccountNumber(),user);
 }
 public Optional  findUserByAccountNumber(String accountNumber){
    Optional<User>user = Optional.ofNullable(userList.get(accountNumber));
return  user;
 }
 public List<User>getAllUser(){
  List<User>users = new ArrayList<>();
  userList.forEach((k,v)->{
      users.add(v);
  });
 return users;
 }

}