package com.example.dagicore_bank_app;

public class MinimumInitialDepositException extends RuntimeException{
    public MinimumInitialDepositException(String message) {
        super(message);
    }
}
