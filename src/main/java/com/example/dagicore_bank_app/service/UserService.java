package com.example.dagicore_bank_app.service;

import com.example.dagicore_bank_app.models.BankDataBase;
import com.example.dagicore_bank_app.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
@Service
public class UserService implements UserDetailsService {
    BankDataBase bankDataBase = new BankDataBase();
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<User> optionalUser = bankDataBase.findUserByAccountNumber(username);

        if(optionalUser.isEmpty()){

        }
        else{

        }
        User user = optionalUser.get();
        System.out.println(user.getAccountName());
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (String userAuthority : user.getRoles().toString().split(",")) {
            SimpleGrantedAuthority simpleGrantedAuthority = new SimpleGrantedAuthority(userAuthority);
            authorities.add(simpleGrantedAuthority);
        }

        return  new org.springframework.security.core.userdetails.User(user.getAccountNumber(),
                user.getAccountPassword(), authorities);
    }
}
