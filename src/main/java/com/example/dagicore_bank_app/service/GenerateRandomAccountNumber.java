package com.example.dagicore_bank_app.service;

import java.util.UUID;

public class GenerateRandomAccountNumber {
    public static String randomAccountNumber(UUID uuid){
        uuid = UUID.randomUUID();
        String id = uuid.toString();
        id = id.substring(26,id.length());
        id = "0x"+id;
        id = (Long.decode(id)).toString();
        id = id.substring(0,10);
   return id;
    }
}
