package com.example.dagicore_bank_app.service;

import com.example.dagicore_bank_app.dto.UserDto;

public interface CreateNewUserService {
    public void createUser(UserDto userDto);

}
