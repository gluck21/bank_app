package com.example.dagicore_bank_app.service.serviceimplementation;

import com.example.dagicore_bank_app.MinimumInitialDepositException;
import com.example.dagicore_bank_app.dto.UserDto;
import com.example.dagicore_bank_app.enums.Roles;
import com.example.dagicore_bank_app.models.BankDataBase;
import com.example.dagicore_bank_app.models.User;
import com.example.dagicore_bank_app.service.CreateNewUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;
@Service
public class CreateNewUserServiceimpl implements CreateNewUserService {
    @Autowired
    PasswordEncoder passwordEncoder;
    @Override
    public void createUser(UserDto userDto) {
        BankDataBase bankDataBase = new BankDataBase();
        String userName = userDto.getAccountName();
        String userPassWord = userDto.getAccountPassword();
        Double initialDeposit = userDto.getInitialDeposit();
        User user = new User();
       UUID uuid = UUID.randomUUID();
        String id = uuid.toString();
        id = id.substring(26,id.length());
        id = "0x"+id;
        id = (Long.decode(id)).toString();
        id = id.substring(0,10);
        user.setAccountNumber(id);
        user.setAccountName(userName);
        user.setRoles(Roles.USER);
        user.setIsEnabled(true);
        user.setAccountPassword(passwordEncoder.encode(userPassWord));
        if(userDto.getInitialDeposit()>=500){
            user.addToBalance(userDto.getInitialDeposit());
        }
        else throw  new MinimumInitialDepositException("initial deposit must be 500 naira and above");
        bankDataBase.addUser(user);

    }
}
