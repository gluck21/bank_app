package com.example.dagicore_bank_app.service.serviceimplementation;

import com.example.dagicore_bank_app.enums.TransactionType;
import com.example.dagicore_bank_app.models.AccountStatement;
import com.example.dagicore_bank_app.models.BankDataBase;
import com.example.dagicore_bank_app.models.User;
import com.example.dagicore_bank_app.service.CustomerDepositService;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
@Service
public class CustomerDepositServiceImpl implements CustomerDepositService {
    BankDataBase bankDataBase = new BankDataBase();
    @Override
    public void customerDeposit(Double amount, String accountNumber) {
   Optional<User> userOptional = bankDataBase.findUserByAccountNumber(accountNumber);
   if(userOptional.isEmpty()){

   }
   else {
       if(amount>1 && amount<1000000){
          User user = userOptional.get();
       List<AccountStatement>accountStatementList= user.getAccountStatementList();
       AccountStatement acoountstatement = new AccountStatement();
       acoountstatement.setTransactionType(TransactionType.DEPOSIT);
       acoountstatement.setTransactionDate(new Date());
       Double newBalance = user.addToBalance(amount);
       acoountstatement.setAccountBalance(newBalance);


       }else{

       }
   }

    }
}
