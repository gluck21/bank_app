package com.example.dagicore_bank_app.service;

import com.example.dagicore_bank_app.dto.WithdrawalDto;

public interface WithdrawalService {
    public void withdraw(Double withdrawAmount, String userAccountNumber);
}
