package com.example.dagicore_bank_app;

import com.example.dagicore_bank_app.dto.LoginDto;

import com.example.dagicore_bank_app.dto.UserDto;
import com.example.dagicore_bank_app.models.BankDataBase;
import com.example.dagicore_bank_app.models.User;
import com.example.dagicore_bank_app.service.CreateNewUserService;
import com.example.dagicore_bank_app.service.CustomerDepositService;
import com.example.dagicore_bank_app.service.serviceimplementation.CustomerDepositServiceImpl;
import com.example.dagicore_bank_app.service.serviceimplementation.WithDrawalServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
public class Controller {
    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    BankDataBase bankDataBase;
    @Autowired
    CreateNewUserService createNewUserService;
    @Autowired
    CustomerDepositServiceImpl  customerDepositService;
    @Autowired
    WithDrawalServiceImpl  withDrawalService;

    @PostMapping("/create")
    public ResponseEntity<?> createUser(@RequestBody UserDto userDto){
        System.out.println(userDto.getInitialDeposit());
        createNewUserService.createUser(userDto);
        return  new ResponseEntity<Object>(HttpStatus.CREATED);}
    @GetMapping("/get_all")
    public ResponseEntity<?> getAllUsers(){
        List<User> userList = bankDataBase.getAllUser();
        return  new ResponseEntity<Object>(userList,HttpStatus.OK);
    }
    @PostMapping("/deposit")
    public  ResponseEntity<?>deposit(@RequestBody Double amount,HttpServletRequest req){
      String accountNumber = req.getUserPrincipal().getName();
      customerDepositService.customerDeposit(amount,accountNumber);
      return new  ResponseEntity<Object>("success",HttpStatus.OK);

    }
    @PostMapping("/withdraw")
    public  ResponseEntity<?>withdraw(@RequestBody Double amount,HttpServletRequest req){
        String accountNumber = req.getUserPrincipal().getName();
        withDrawalService.withdraw(amount,accountNumber);
        return new  ResponseEntity<Object>("success",HttpStatus.OK);

    }
    @GetMapping("/account_info/{accountNumber}")
    public ResponseEntity<?> getuserInfo(@PathVariable String accountNumber ){
Optional<User> optionalUser = bankDataBase.findUserByAccountNumber(accountNumber);
if(optionalUser.isEmpty()) throw  new RuntimeException();
else{
    User user = optionalUser.get();
    return  new ResponseEntity<Object>(user,HttpStatus.OK);
}

    }

}
